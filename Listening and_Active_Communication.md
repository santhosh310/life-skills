# Listening and Active Communication

## What are the steps/strategies to do Active Listening?

- Face the speaker and have eye contact : Eye contact is an important part of face to face conversation.
-  Don’t start planning what to say next : 
You can’t listen and prepare at the same time
- Stay focused If you're finding it difficult to focus on what someone is saying, try repeating their words in your head as they say them – this will reinforce what they’re saying and help you to concentrate. 
-  Ask questions Asking relevant questions can show that you’ve been listening and help clarify what has been said.
- Ask probing questions by asking questions your mind will get comfortable and we can move on.
- Ask open-ended questions this is also one starategies for active listening.



## According to Fisher's model, what are the key points of Reflective Listening?

By engaging in a non-judgmental and empathetic approach, listeners encourage the others to speak freely. Mirroring the mood of the speaker, reflecting the emotional state with words and nonverbal communication. This requires the listener to quiet his mind and focus fully upon the mood of the speaker. This makes the listener more focus on listening to the speaker.

## What are the obstacles in your listening process?

Self-centeredness. This causes me to focus on his or her own thoughts rather than the speaker’s words. I can become impatient with a speaker who talks slowly or draws out the message.These are the obsatacles in my listening process.

## What can you do to improve your listening?

Face the speaker and maintain eye contact,Be attentive, but relaxed,Keep an open mind,Listen to the words and try to picture what the speaker is saying,Don't interrupt and don't impose your "solutions,Wait for the speaker to pause to ask clarifying questions these are the things we need to do for improving active listing.

## When do you switch to Passive communication style in your day to day life?

Passive and aggressive communication might work better on some occasions. For example, if you are feeling fearful that you are about to be harmed, passive communication may help to defuse the situation and aggressive communication might prevent the problem from getting worse.

## When do you switch into Aggressive communication styles in your day to day life?

Passive and aggressive communication might work better on some occasions. For example, 
_if you are feeling fearful that you are about to_ be harmed, passive communication may help to defuse the situation and aggressive communication might prevent the problem from getting worse.




## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

Passive and aggressive communication might work better on some occasions. For example, if you are feeling fearful that you are about to be harmed, passive communication may help to defuse the situation and aggressive communication might prevent the problem from getting worse.

## How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)


Ask someone else's opinion, then listen to the answer. When you disagree, try to say so without putting down the other person's point of view. For example, instead of saying: "That's a stupid idea," try: "I don't really like that idea." Or instead of saying: "He's such a jerk," try: "I think he's insensitive."

